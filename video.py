#!/usr/bin/env python3

# ISC License
#
# Copyright (c) 2019, Anthonix <anthonix@autistici.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

config_file = "aristarc"

import cgi
import cgitb
cgitb.enable(display=0)

# parse config
import configparser
config = configparser.ConfigParser()
config.read(config_file)

style = config['arista']['style']
host = config['arista']['host']
language = config['arista']['language']

instance_list = config['video']['instance_list']
path = config['video']['path']
title = config['video']['title']
description = config['video']['description']
complete_url = config['video']['complete_url']

alt_title = title.lower().replace(" ", "-")

# get entries
entry = cgi.FieldStorage()

def header():
   print("Status: 200 OK\r")
   print("Content-Type: text/html\r")
   print("\r")

def head():
   header()
   print("<!DOCTYPE html>")
   print(f"<html lang=\"{language}\">")
   print("  <head>")
   print("    <meta charset=\"utf-8\">")
   print(f"    <title>{title}</title>")
   print(f"    <meta name=\"description\" content=\"{description}\">")
   print(f"    <link href=\"{style}\" rel=\"stylesheet\" type=\"text/css\">")
   print("    <link rel=\"search\" type=\"application/opensearchdescription+xml\"")
   print(f"          href=\"{path}?{alt_title}=osd\" title=\"{title}\">")
   print("  </head>")
   print("  <body>")
   print("    <header>")
   print(f"      <h1>{title}</h1>")
   print("    </header>")
   print("    <main>")
   print("      <p id=\"center\">")
   print(f"        {description}")
   print("      </p>")

# check validity of entry
if entry:
   if entry.getvalue('q') is None and entry.getvalue(alt_title) is None and entry.getvalue('v') is None:
      head()
      print("      <ul id=\"error\">")
      print("        <li>Query cannot be Null</li>")
      print("      </ul>")
   elif entry.getvalue(alt_title) == "osd":
      header()
      print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
      print("<OpenSearchDescription xmlns=\"http://a9.com/-/spec/opensearch/1.1/\">")
      print(f"  <ShortName>{title}</ShortName>")
      print(f"  <Description>{description}</Description>")
      print("  <Url type=\"text/html\" method=\"get\"")
      print(f"       template=\"{complete_url}?q={{searchTerms}}\"/>")
      print("</OpenSearchDescription>")
      exit(0)
   elif entry.getvalue('v'):
      import urllib
      vid_id = urllib.parse.quote_plus(entry.getvalue('v').strip().encode())
      len_vid_id = len(vid_id)
      if len_vid_id > 64 or len_vid_id < 1:
         head()
         print("      <ul id=\"error\">")
         print("        <li>Video id should be between 1 & 64 characters</li>")
         print("      </ul>")
      else:
         import random
         with open(instance_list, 'r') as data:
            instances = [line.strip() for line in data]
         print("Status: 302 Found\r")
         print(f"Location: https://{random.choice(instances)}/watch?v={vid_id}&hl={language}")
         print("\r")
         exit(0)
   else:
      import urllib
      query = urllib.parse.quote_plus(entry.getvalue('q').strip().encode())
      len_query = len(query)
      if len_query > 2048 or len_query < 1:
         head()
         print("      <ul id=\"error\">")
         print("        <li>Query should be between 1 & 2048 characters</li>")
         print("      </ul>")
      else:
         import random
         with open(instance_list, 'r') as data:
            instances = [line.strip() for line in data]
         print("Status: 302 Found\r")
         print(f"Location: https://{random.choice(instances)}/search?q={query}&hl={language}")
         print("\r")
         exit(0)
else:
   head()

print(f"      <form action=\"{path}\" method=\"post\">")
print("        <input id=\"query\" type=\"text\" maxlength=2048 name=\"q\" required>")
print("        <input id=\"submit\" type=\"submit\" name=\"submit\" value=\"Search!\">")
print("      </form>")
print("      <br>")
print("      <ul id=\"success\">")
print("        <li>No logs</li>")
print("        <li>Free/Libre Software</li>")
print("      </ul>")
print("      <p id=\"center-small\">")
print("        <a href=\"https://liberapay.com/omarroth\">support invidious</a> &#183;")
print("        <a href=\"https://git.sr.ht/~anthonix/arista\">source code</a>")
print("      </p>")
print("    </main>")
print("    <footer>")
print(f"      <a href=\"{path}\">{alt_title}</a>")
print("    </footer>")
print("  </body>")
print("</html>")
exit(0)
